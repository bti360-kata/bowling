package com.bti360.bowling;

public interface RecordKeeper {
	boolean isRecord(int score);
	
	void announceScore(int score);
}
