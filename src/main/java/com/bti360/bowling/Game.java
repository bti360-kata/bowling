package com.bti360.bowling;

/**
 * A bowling game consists of 10 frames. In each frame, the player has two tries to knock down 10 pins.
 * 
 * The score for one frame is the total number of pins knocked down, plus bonuses for strikes and spares.
 * 
 * A spare is when the player knocks down all 10 pins in two tries.
 * 
 * A strike is when the player knocks down all 10 pins on his/her first try.
 * 
 * When a spare is rolled, that frame receives a bonus equal to the value of the next ball rolled.
 * 
 * When a strike is rolled, that frame receives a bonus equal to the next two balls rolled.
 * 
 * If a player tolls a spare or strike in the 10th frame, an additional ball is rolled.
 * 
 * No more than three balls can be rolled in the 10th frame.
 */
public class Game {

	/**
	 * Called each time the player rolls a boll.
	 * 
	 * @param pins number of pins knocked down by the roll.
	 */
	public void roll(int pins) {
		
	}
	
	/**
	 * Called only at the very end of the game.
	 * 
	 * @return the total score for the game.
	 */
	public int score() {
		return -1;
	}
}
